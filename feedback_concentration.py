#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#insert csv file here
df= pd.read_csv('26 August AKTU mid SELP Feedback form (Responses) - Form Responses 1.csv')

df_concentration = df[['College Name','Increased Concentration Level']]

df_concentration_cross = pd.crosstab(df_concentration['College Name'],df_concentration['Increased Concentration Level'])

# df_concentration_cross

df_concentration_cross['May be'] = df_concentration_cross[1]+ df_concentration_cross[2]+ df_concentration_cross[3]+ df_concentration_cross[4]+df_concentration_cross[5]
df_concentration_cross['Agree'] = df_concentration_cross[6] + df_concentration_cross[7]+df_concentration_cross[8]
df_concentration_cross['Strongly Agree'] = df_concentration_cross[9]+ df_concentration_cross[10]

# df_concentration_cross

concentration_cross_sort = df_concentration_cross[['May be','Agree','Strongly Agree']]

concentration_sort= concentration_cross_sort.T
# concentration_sort

# concentration_sort['BBD Lucknow'].plot.pie(subplots=True,startangle=90, figsize=(5, 5))

#repeat following steps for every college
plt.pie(concentration_sort['AITH'],labels=concentration_sort.index,
startangle=90,counterclock=False, autopct='%1.0f%%')
plt.title('Increased Concentration Level', fontweight="bold")
plt.rcParams.update({'font.size': 14})
plt.rcParams["font.weight"] = "bold"
plt.savefig('AITH_concentration.png')
plt.show()


# In[ ]:




