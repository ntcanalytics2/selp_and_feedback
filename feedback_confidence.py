#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#insert pdf file here
df= pd.read_csv('26 August AKTU mid SELP Feedback form (Responses) - Form Responses 1.csv')

df_confidence = df[['College Name','Improved Confidence Level']]

df_confidence_cross = pd.crosstab(df_confidence['College Name'],df_confidence['Improved Confidence Level'])

# df_confidence_cross

df_confidence_cross['May be'] = df_confidence_cross[1]+ df_confidence_cross[2]+ df_confidence_cross[3]+ df_confidence_cross[4]+df_confidence_cross[5]
df_confidence_cross['Agree'] = df_confidence_cross[6] + df_confidence_cross[7]+df_confidence_cross[8]
df_confidence_cross['Strongly Agree'] = df_confidence_cross[9]+ df_confidence_cross[10]

# df_confidence_cross

confidence_cross_sort = df_confidence_cross[['May be','Agree','Strongly Agree']]

confidence_sort= confidence_cross_sort.T
# confidence_sort

# concentration_sort['BBD Lucknow'].plot.pie(subplots=True,startangle=90, figsize=(5, 5))

#Repeat follwing codes for every college
plt.pie(confidence_sort['AITH'],labels=confidence_sort.index,
startangle=90,counterclock=False, autopct='%1.0f%%')
plt.title('Improved Confidence Level',fontweight="bold")
plt.rcParams.update({'font.size': 12})
plt.rcParams["font.weight"] = "bold"
plt.savefig('AITH_confidence.png')
plt.show()


# In[ ]:




