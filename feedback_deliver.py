#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#insert pdf file here
df= pd.read_csv('26 August AKTU mid SELP Feedback form (Responses) - Form Responses 1.csv')

df_deliver = df[['College Name','Ability to deliver program content']]

df_deliver_cross = pd.crosstab(df_deliver['College Name'],df_deliver['Ability to deliver program content'])

# df_deliver_cross

df_deliver_cross['May be'] = df_deliver_cross[1]+ df_deliver_cross[2]+ df_deliver_cross[4]+df_deliver_cross[5]
df_deliver_cross['Agree'] = df_deliver_cross[6] + df_deliver_cross[7]+df_deliver_cross[8]
df_deliver_cross['Strongly Agree'] = df_deliver_cross[9]+ df_deliver_cross[10]

# df_deliver_cross

deliver_cross_sort = df_deliver_cross[['May be','Agree','Strongly Agree']]

deliver_sort= deliver_cross_sort.T
# deliver_sort

# concentration_sort['BBD Lucknow'].plot.pie(subplots=True,startangle=90, figsize=(5, 5))

#Repeat following codes for every college
plt.pie(deliver_sort['AITH'],labels=deliver_sort.index,
startangle=90,counterclock=False, autopct='%1.0f%%')
plt.title('Ability to deliver program content',fontweight="bold")
plt.rcParams.update({'font.size': 12})
plt.rcParams["font.weight"] = "bold"
plt.savefig('AITH_deliver.png')
plt.show()

