#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#insert pdf file here
df= pd.read_csv('26 August AKTU mid SELP Feedback form (Responses) - Form Responses 1.csv')

df_happymind = df[['College Name','More clear, calm and a happy state of mind']]

df_happymind_cross = pd.crosstab(df_happymind['College Name'],df_happymind['More clear, calm and a happy state of mind'])

# df_happymind_cross

df_happymind_cross['May be'] = df_happymind_cross[1]+ df_happymind_cross[3]+ df_happymind_cross[4]+df_happymind_cross[5]
df_happymind_cross['Agree'] = df_happymind_cross[6] + df_happymind_cross[7]+df_happymind_cross[8]
df_happymind_cross['Strongly Agree'] = df_happymind_cross[9]+ df_happymind_cross[10]

# df_happymind_cross

happymind_cross_sort = df_happymind_cross[['May be','Agree','Strongly Agree']]

happymind_sort= happymind_cross_sort.T
# happymind_sort

# concentration_sort['BBD Lucknow'].plot.pie(subplots=True,startangle=90, figsize=(5, 5))

#Repeat following codes for every college
plt.pie(happymind_sort['AITH'],labels=happymind_sort.index,
startangle=90,counterclock=False, autopct='%1.0f%%')
plt.title('More clear, calm and a happy state of mind',fontweight="bold")
plt.rcParams.update({'font.size': 12})
plt.rcParams["font.weight"] = "bold"
plt.savefig('AITH_happymind.png')
plt.show()

