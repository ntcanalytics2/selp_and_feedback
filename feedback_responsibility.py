#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#insert pdf file here
df= pd.read_csv('26 August AKTU mid SELP Feedback form (Responses) - Form Responses 1.csv')

df_responsibility = df[['College Name','Stronger Sense of Responsibility']]

df_responsibility_cross = pd.crosstab(df_responsibility['College Name'],df_responsibility['Stronger Sense of Responsibility'])

# df_responsibility_cross

df_responsibility_cross['May be'] = df_responsibility_cross[1]+ df_responsibility_cross[2]+ df_responsibility_cross[3]+ df_responsibility_cross[4]+df_responsibility_cross[5]
df_responsibility_cross['Agree'] = df_responsibility_cross[6] + df_responsibility_cross[7]+df_responsibility_cross[8]
df_responsibility_cross['Strongly Agree'] = df_responsibility_cross[9]+ df_responsibility_cross[10]

# df_responsibility_cross

responsibility_cross_sort = df_responsibility_cross[['May be','Agree','Strongly Agree']]

responsibility_sort= responsibility_cross_sort.T
# responsibility_sort

# concentration_sort['BBD Lucknow'].plot.pie(subplots=True,startangle=90, figsize=(5, 5))

#Repeat following codes for every college
plt.pie(responsibility_sort['AITH'],labels=responsibility_sort.index,
startangle=90,counterclock=False, autopct='%1.0f%%')
plt.title('Stronger Sense of Responsibility',fontweight="bold")
plt.rcParams.update({'font.size': 12})
plt.rcParams["font.weight"] = "bold"
plt.savefig('AITH_responsibility.png')
plt.show()

