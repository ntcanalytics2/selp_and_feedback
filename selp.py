#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pylab as plt
import seaborn as sns
import numpy as np
from scipy.stats import ttest_rel

#insert your pre-analysis file here
pre_df = pd.read_csv('BIET_pre.csv')
#insert your post-analysis file here
post_df = pd.read_csv('BIET_post.csv')

pre_df['Phone Number'] = pre_df['Phone Number'].astype(str).str[-10:]
post_df['Phone Number'] = post_df['Phone Number'].astype(str).str[-10:]
pre_df

PANAS_columns=['Interested', 'Distressed', 'Excited', 'Upset',
       'Strong', 'Guilty', 'Scared', 'Hostile', 'Enthusiastic', 'Proud',
       'Irritable', 'Alert', 'Ashamed', 'Inspired', 'Nervous', 'Determined',
       'Attentive', 'Jittery', 'Active', 'Afraid']
len(PANAS_columns)

PANAS_positive = ['Interested', 'Excited',
       'Strong', 'Enthusiastic', 'Proud',
       'Alert', 'Inspired','Determined',
       'Attentive', 'Active']
PANAS_negative = ['Distressed','Upset', 'Scared',
       'Guilty','Hostile',
       'Irritable',  'Ashamed', 'Nervous',  'Jittery','Afraid',]
len(PANAS_negative), len(PANAS_positive)

pre_df.Interested.value_counts(), post_df.Interested.value_counts()

pre_PANAS_df = pre_df.loc[:,PANAS_columns]
post_PANAS_df = post_df.loc[:,PANAS_columns]
post_PANAS_df

pre_PANAS_freq_df = pre_PANAS_df.apply(pd.value_counts)
post_PANAS_freq_df = post_PANAS_df.apply(pd.value_counts)
post_PANAS_freq_df

pre_PANAS_freq_df

pre_PANAS_positive_freq_df = pre_PANAS_freq_df.loc[:,PANAS_positive]
pre_PANAS_negative_freq_df = pre_PANAS_freq_df.loc[:,PANAS_negative]
post_PANAS_positive_freq_df = post_PANAS_freq_df.loc[:,PANAS_positive]
post_PANAS_negative_freq_df = post_PANAS_freq_df.loc[:,PANAS_negative]
print(pre_PANAS_positive_freq_df)
print(pre_PANAS_negative_freq_df)
print(post_PANAS_positive_freq_df)
print(post_PANAS_negative_freq_df)

pre_unique_cellno_df = pre_df.drop_duplicates(subset=['Phone Number']).set_index('Phone Number', verify_integrity=True) #.reindex(post_df['Cell Number']).drop_duplicates()
post_unique_cellno_df = post_df.drop_duplicates(subset=['Phone Number']).set_index('Phone Number', verify_integrity=True) #.reindex(post_df['Cell Number']).drop_duplicates()
post_unique_cellno_df

pre_unique_cellno_df

pre_unique_cellno_df.shape, post_unique_cellno_df.shape

pre_unique_cellno_df= pre_unique_cellno_df.reindex(post_unique_cellno_df.index)
pre_unique_cellno_df

# pre_bool_series = 
pre_unique_cellno_positive_df = pre_unique_cellno_df.loc[:,PANAS_positive]
pre_unique_cellno_positive_df[pre_unique_cellno_positive_df<=3].apply(np.nanmean).mean()

post_unique_cellno_positive_df = post_unique_cellno_df.loc[:,PANAS_positive]
post_unique_cellno_positive_df[pre_unique_cellno_positive_df<=3].apply(np.nanmean).mean()

# pre_bool_series = 
pre_unique_cellno_negative_df = pre_unique_cellno_df.loc[:,PANAS_negative]
pre_unique_cellno_negative_df[pre_unique_cellno_negative_df>3].apply(np.nanmean).mean()

post_unique_cellno_negative_df = post_unique_cellno_df.loc[:,PANAS_negative]
# post_unique_cellno_negative_df
post_unique_cellno_negative_df[pre_unique_cellno_negative_df>3].apply(np.nanmean).mean()

mean_diff_high_negative_df = pd.concat([pre_unique_cellno_negative_df[pre_unique_cellno_negative_df>3].apply(np.nanmean).rename('pre'),
        post_unique_cellno_negative_df[pre_unique_cellno_negative_df>3].apply(np.nanmean).rename('post')], 
         axis='columns') #.plot(kind='bar')
# mean_diff_high_negative_df['precentage change']=100*(mean_diff_high_negative_df.pre-mean_diff_high_negative_df.post)/mean_diff_high_negative_df.pre
# mean_diff_high_negative_df.to_csv('BIET_mean_diff_high_negative_df.csv')
mean_diff_high_negative_df

f, (ax, ax2) = plt.subplots(2, 1, sharex=True, figsize=(15,10),gridspec_kw={'height_ratios': [3, 1]})
mean_diff_high_negative_df.plot(ax=ax, kind='bar')
mean_diff_high_negative_df.plot(ax=ax2, kind='bar')
ax.set_ylim(0.9, 5)  # outliers only
ax2.set_ylim(0, 0.4)  # most of the data
ax.spines['bottom'].set_visible(False)
ax2.spines['top'].set_visible(False)
d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal
dummy=plt.setp(ax.xaxis.get_majorticklabels(), rotation=75)
plt.show()

# mean_diff_high_negative_df = pd.concat([pre_unique_cellno_negative_df[pre_unique_cellno_negative_df>3].apply(np.nanmean).rename('Pre'), 
#         post_unique_cellno_negative_df[pre_unique_cellno_negative_df>3].apply(np.nanmean).rename('Posttest'),], 
#          axis='columns') #.plot(kind='bar')
# # mean_diff_low_positive_df['precentage change']=100*(mean_diff_low_positive_df.post-mean_diff_low_positive_df.pre)/mean_diff_low_positive_df.pre
# # mean_diff_high_negative_df['precentage change']=100*(mean_diff_negative_df['Pretest']-mean_diff_negative_df['Posttest'])/mean_diff_negative_df['Pretest']
# mean_diff_high_negative_df

f, (ax, ax2) = plt.subplots(2, 1, sharex=True, figsize=(15,10),gridspec_kw={'height_ratios': [3, 1]})
mean_diff_high_negative_df.plot(ax=ax, kind='bar')
mean_diff_high_negative_df.plot(ax=ax2, kind='bar').get_legend().remove()
ax.set_ylim(1.7, 5)  # outliers only
ax2.set_ylim(0, 0.4)  # most of the data
ax.spines['bottom'].set_visible(False)
ax2.spines['top'].set_visible(False)
# ax.xaxis.tick_top()
# ax.tick_params(labeltop='off')  # don't put tick labels at the top
# ax2.xaxis.tick_bottom()
d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
# kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
# ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
# ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

# kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
# d2=d
# ax2.plot((-d2, +d2), (1 - d2, 1 + d2), **kwargs)  # bottom-left diagonal
# ax2.plot((1 - d2, 1 + d2), (1 - d2, 1 + d2), **kwargs)  # bottom-right diagonal
# mean_pre = mean_diff_high_negative_df['pre'].mean()
# ax.axhline(mean_pre, color='red', linewidth=2, linestyle='--') #showing pre-mean line in the plot
# mean_post = mean_diff_high_negative_df['post'].mean()
# ax.axhline(mean_post, color='cyan', linewidth=2, linestyle='--') #showing post-mean line in the plot
dummy=plt.setp(ax.xaxis.get_majorticklabels(), rotation=75)
plt.xticks(rotation=0)
plt.subplots_adjust(wspace=0, hspace=0.01)
plt.rcParams.update({'font.size': 18})
plt.savefig('BIET_PANAS_pre_post_high_negative_stacked_bar.png')
plt.show()

import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
# plt.figure(figsize=(15,8))
fig = mean_diff_high_negative_df.mean().plot(kind='bar', color=['red','dodgerblue'])
plt.margins(0.02)
fig.set_xticklabels(mean_diff_high_negative_df.columns, rotation = 0)
plt.title('Impact on Negative Emotions',fontweight="bold")
plt.rcParams.update({'font.size': 16})
plt.rcParams["font.weight"] = "bold"
plt.savefig('Impact on Negative Emotions.png')
plt.show()

mean_diff_low_positive_df = pd.concat([pre_unique_cellno_positive_df[pre_unique_cellno_positive_df<=3].apply(np.nanmean).rename('pre'), 
        post_unique_cellno_positive_df[pre_unique_cellno_positive_df<=3].apply(np.nanmean).rename('post'),], 
         axis='columns') #.plot(kind='bar')
# mean_diff_low_positive_df['precentage change']=100*(mean_diff_low_positive_df.post-mean_diff_low_positive_df.pre)/mean_diff_low_positive_df.pre
# mean_diff_low_positive_df.to_csv('BIET_mean_diff_low_positive_df.csv')
mean_diff_low_positive_df

f, (ax, ax2) = plt.subplots(2, 1, sharex=True, figsize=(15,10),gridspec_kw={'height_ratios': [3, 1]})
mean_diff_low_positive_df.plot(ax=ax, kind='bar')
mean_diff_low_positive_df.plot(ax=ax2, kind='bar').get_legend().remove()
ax.set_ylim(1.4, 5)  # outliers only
ax2.set_ylim(0, 0.4)  # most of the data
ax.spines['bottom'].set_visible(False)
ax2.spines['top'].set_visible(False)
# ax.xaxis.tick_top()
# ax.tick_params(labeltop='off')  # don't put tick labels at the top
# ax2.xaxis.tick_bottom()
d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
# kwargs = dict(transform=ax.transAxes, color='g', clip_on=False)
# ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
# ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

# kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
# d2=d*1.2
# ax2.plot((-d2, +d2), (1 - d2, 1 + d2), **kwargs)  # bottom-left diagonal
# ax2.plot((1 - d2, 1 + d2), (1 - d2, 1 + d2), **kwargs)  # bottom-right diagonal
dummy=plt.setp(ax.xaxis.get_majorticklabels(), rotation=75)
plt.xticks(rotation=0)
plt.subplots_adjust(wspace=0, hspace=0.01)
plt.rcParams.update({'font.size': 22})
plt.savefig('BIET_PANAS_pre_post_low_positive_stacked_bar.png')
plt.show()

import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
# plt.figure(figsize=(15,8))
fig = mean_diff_low_positive_df.mean().plot(kind='bar', color=['red','dodgerblue'])
plt.margins(0.02)
fig.set_xticklabels(mean_diff_low_positive_df.columns, rotation = 0)
plt.title('Impact on Positive Emotions',fontweight="bold")
plt.rcParams.update({'font.size': 16})
plt.rcParams["font.weight"] = "bold"
plt.savefig('Impact on Positive Emotions.png')
plt.show()

MAAS_columns=[
    'I could be experiencing some emotion and not be conscious of it until some time later.',
    'I break or spill things because of carelessness, not paying attention, or thinking of something else.',
    'I find it difficult to stay focused on what’s happening in the present.',
    'I tend to walk quickly to get where I’m going without paying attention to what I experience along the way.',
    'I tend not to notice feelings of physical tension or discomfort until they really grab my attention.',
    'I forget a person’s name almost as soon as I’ve been told it for the first time.',
    'It seems I am “running on automatic,” without much awareness of what I’m doing.',
    'I rush through activities without being really attentive to them.',
    'I get so focused on the goal I want to achieve that I lose touch with what I’m doing right now to get there.',
    "I do jobs or tasks automatically, without being aware of what I'm doing.",
    'I find myself listening to someone with one ear, doing something else at the same time.',
    'I drive places on ‘automatic pilot’ and then wonder why I went there',
    'I find myself preoccupied with the future or the past',
    'I find myself doing things without paying attention.',
    'I snack without being aware that I’m eating.'
    
]

MAAS_mapping = {'somewhat frequently':3,
'very frequently':2,
'Almost Always':1,
'somewhat Infrequently':4,
'very Infrequently':5,
'almost never':6}

pre_unique_cellno_MAAS_numerical_df = pre_unique_cellno_df.loc[:,MAAS_columns].replace(MAAS_mapping)
pre_unique_cellno_MAAS_numerical_df

post_unique_cellno_MAAS_numerical_df = post_unique_cellno_df.loc[:,MAAS_columns].replace(MAAS_mapping)
post_unique_cellno_MAAS_numerical_df

pre_unique_cellno_MAAS_numerical_df.apply(np.nanmean, axis='columns').mean()

post_unique_cellno_MAAS_numerical_df.mean(axis='columns').mean()

ttest_rel(pre_unique_cellno_MAAS_numerical_df.mean(axis='columns'),
         post_unique_cellno_MAAS_numerical_df.mean(axis='columns'),
         nan_policy='omit')

# MAAS_graph_df = pd.DataFrame([pre_unique_cellno_MAAS_numerical_df.mean(axis='columns').mean(), post_unique_cellno_MAAS_numerical_df.mean(axis='columns').mean()]) 
# MAAS_graph_df.index=(['Pre', 'Post']) 
# MAAS_graph_df.plot(kind='bar')

MAAS_graph_df = pd.DataFrame([pre_unique_cellno_MAAS_numerical_df.mean(axis='columns').mean(),post_unique_cellno_MAAS_numerical_df.mean(axis='columns').mean()]) 
MAAS_graph_df.index=(['Pre', 'Post']) 
MAAS_graph_df.plot(kind='bar')

f, (ax, ax2) = plt.subplots(2, 1, sharex=True, figsize=(15,10),gridspec_kw={'height_ratios': [3, 1]})
MAAS_graph_df.plot(ax=ax, kind='bar').get_legend().remove()
MAAS_graph_df.plot(ax=ax2, kind='bar').get_legend().remove()
ax.set_ylim(3.2, 4.4)  # outliers only
ax2.set_ylim(0, 1)  # most of the data
ax.spines['bottom'].set_visible(False)
ax2.spines['top'].set_visible(False)
# ax.xaxis.tick_top()
# ax.tick_params(labeltop='off')  # don't put tick labels at the top
# ax2.xaxis.tick_bottom()
d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
# kwargs = dict(transform=ax.transAxes, color='g', clip_on=False)
# ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
# ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

# kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
# d2=d*1.2
# ax2.plot((-d2, +d2), (1 - d2, 1 + d2), **kwargs)  # bottom-left diagonal
# ax2.plot((1 - d2, 1 + d2), (1 - d2, 1 + d2), **kwargs)  # bottom-right diagonal
dummy=plt.setp(ax.xaxis.get_majorticklabels(), rotation=75)
plt.xticks(rotation=0)
plt.subplots_adjust(wspace=0, hspace=0.05)
plt.rcParams.update({'font.size': 16})
plt.savefig('BIET_MAAS_stacked_bar.png')
plt.show()

stress_questionaire = [
    'I frequently bring work home at night',
       'Not enough hours in the day to do all the things that I must do',
       'I deny or ignore problems in the hope that they will go away',
       'I do the jobs myself to ensure they are done properly',
       'I underestimate how long it takes to do things',
       'I feel that there are too many deadlines in my work / life that are difficult to meet',
       'My self confidence / self esteem is lower than I would like it to be',
       'I frequently have guilty feelings if I relax and do nothing',
       'I find myself thinking about problems even when I am supposed to be relaxing',
       'I feel fatigued or tired even when I wake after an adequate sleep',
       'I often nod or finish other peoples sentences for them when they speak slowly',
       'I have a tendency to eat, talk, walk and drive quickly',
       'My appetite has changed, have either a desire to binge or have a loss of appetite / may skip meals',
       'I feel irritated or angry if the car or traffic in front seems to be going too slowly/I become very frustrated at having to wait in a queue',
       'If something or someone really annoys me I will bottle up my feelings',
       'When I play sport or games, I really try to win whoever I play',
       'I experience mood swings, difficulty making decisions, concentration and memory is impaired',
       'I find fault and criticize others rather than praising, even if it is deserved',
       'I seem to be listening even though I am preoccupied with my own thoughts',
       'My sex drive is lower, can experience changes to menstrual cycle',
       'I find myself grinding my teeth',
       'Increase in muscular aches and pains especially in the neck, head, lower back, shoulders',
       'I am unable to perform tasks as well as I used to, my judgment is clouded or not as good as it was',
       'I find I have a greater dependency on alcohol, caffeine, nicotine or drugs',
       'I find that I don’t have time for many interests / hobbies outside of work',
]

pre_unique_cellno_df.loc[:,stress_questionaire].replace({'Yes':1, 'No':0}).sum(axis='columns').mean()

post_unique_cellno_df.loc[:,stress_questionaire].replace({'Yes':1, 'No':0}).sum(axis='columns').mean()


# In[ ]:





# In[ ]:




