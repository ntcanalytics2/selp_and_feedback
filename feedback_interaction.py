#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#insert pdf file here
df= pd.read_csv('26 August AKTU mid SELP Feedback form (Responses) - Form Responses 1.csv')

df_interaction = df[['College Name','Interaction with participants']]

df_interaction_cross = pd.crosstab(df_interaction['College Name'],df_interaction['Interaction with participants'])

# df_interaction_cross

df_interaction_cross['May be'] = df_interaction_cross[1]+ df_interaction_cross[2]+ df_interaction_cross[3]+ df_interaction_cross[4]+df_interaction_cross[5]
df_interaction_cross['Agree'] = df_interaction_cross[6] + df_interaction_cross[7]+df_interaction_cross[8]
df_interaction_cross['Strongly Agree'] = df_interaction_cross[9]+ df_interaction_cross[10]

# df_interaction_cross

interaction_cross_sort = df_interaction_cross[['May be','Agree','Strongly Agree']]

interaction_sort= interaction_cross_sort.T
# interaction_sort

# concentration_sort['BBD Lucknow'].plot.pie(subplots=True,startangle=90, figsize=(5, 5))

#Repeat following codes for every college
plt.pie(interaction_sort['AITH'],labels=interaction_sort.index,
startangle=90,counterclock=False, autopct='%1.0f%%')
plt.title('Interaction with participants',fontweight="bold")
plt.rcParams.update({'font.size': 12})
plt.rcParams["font.weight"] = "bold"
plt.savefig('AITH_interaction.png')
plt.show()

