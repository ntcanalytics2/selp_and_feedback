#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#insert pdf file here
df= pd.read_csv('26 August AKTU mid SELP Feedback form (Responses) - Form Responses 1.csv')

df_creativity = df[['College Name','Enhanced Creativity']]

df_creativity_cross = pd.crosstab(df_creativity['College Name'],df_creativity['Enhanced Creativity'])

# df_creativity_cross

df_creativity_cross['May be'] = df_creativity_cross[1]+ df_creativity_cross[2]+ df_creativity_cross[3]+ df_creativity_cross[4]+df_creativity_cross[5]
df_creativity_cross['Agree'] = df_creativity_cross[6] + df_creativity_cross[7]+df_creativity_cross[8]
df_creativity_cross['Strongly Agree'] = df_creativity_cross[9]+ df_creativity_cross[10]

# df_creativity_cross

creativity_cross_sort = df_creativity_cross[['May be','Agree','Strongly Agree']]

creativity_sort= creativity_cross_sort.T
# creativity_sort

# concentration_sort['BBD Lucknow'].plot.pie(subplots=True,startangle=90, figsize=(5, 5))

#Repeat follwing codes for every college
plt.pie(creativity_sort['AITH'],labels=creativity_sort.index,
startangle=90,counterclock=False, autopct='%1.0f%%')
plt.title('Enhanced Creativity',fontweight="bold")
plt.rcParams.update({'font.size': 12})
plt.rcParams["font.weight"] = "bold"
plt.savefig('AITH_creativity.png')
plt.show()

